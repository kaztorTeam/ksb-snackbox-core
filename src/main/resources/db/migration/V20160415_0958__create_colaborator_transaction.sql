create sequence colaborator_seq;

create table colaborator (
  id bigint not null default nextval('colaborator_seq'),
  name varchar(100) not null,
  phone varchar(15) not null,
  constraint colaborator_pk primary key (id)
);

create sequence transaction_seq;

create table transaction (
  id bigint not null default nextval('transaction_seq'),
  amount numeric(7,2) not null,
  transaction_date date not null,
  reference_date date,
  description varchar(100),
  colaborator_id bigint not null,
  constraint transaction_pk primary key (id),
  constraint transaction_colaborator foreign key (colaborator_id) references colaborator
);
