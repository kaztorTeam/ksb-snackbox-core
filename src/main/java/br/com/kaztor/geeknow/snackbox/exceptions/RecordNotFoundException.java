package br.com.kaztor.geeknow.snackbox.exceptions;

public class RecordNotFoundException extends Exception {

	private static final long serialVersionUID = 2758560300161994966L;

	public RecordNotFoundException(String message) {
		super(message);
	}

}
