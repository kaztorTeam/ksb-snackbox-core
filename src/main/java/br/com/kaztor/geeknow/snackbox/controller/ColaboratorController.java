package br.com.kaztor.geeknow.snackbox.controller;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.kaztor.geeknow.snackbox.dto.CreateUpdateColaboratorDTO;
import br.com.kaztor.geeknow.snackbox.exceptions.RecordNotFoundException;
import br.com.kaztor.geeknow.snackbox.model.Colaborator;
import br.com.kaztor.geeknow.snackbox.service.SnackBoxService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@Api(tags = "colaborator")
@RestController
@RequestMapping("/colaborator")
public class ColaboratorController {

	@Resource
	private SnackBoxService caixinhaService;

	@ApiOperation(value = "Save a new Colaborator", code = 201)
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Colaborator> create(
			@RequestBody @Valid CreateUpdateColaboratorDTO colaboratorDTO) {
		Colaborator colaborator = new Colaborator();
		BeanUtils.copyProperties(colaboratorDTO, colaborator);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(this.caixinhaService.createColaborator(colaborator));
	}

	@ApiOperation(value = "Update a Colaborator identified by the ID")
	@RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	public @ResponseBody ResponseEntity<Colaborator> update(@PathVariable Long id,
			@RequestBody @Valid CreateUpdateColaboratorDTO colaboratorDTO)
			throws RecordNotFoundException {
		Colaborator colaborator = this.caixinhaService.getColaborator(id);
		BeanUtils.copyProperties(colaboratorDTO, colaborator);

		return ResponseEntity.ok(this.caixinhaService.updateColaborator(colaborator));
	}

	@ApiOperation(value = "Delete a Colaborator identified by the ID")
	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		this.caixinhaService.deleteColaborator(id);
		return ResponseEntity.ok().build();
	}

	@ApiOperation(value = "Get a Colaborator identified by the ID")
	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public @ResponseBody ResponseEntity<Colaborator> get(@PathVariable Long id) throws RecordNotFoundException {
		return ResponseEntity.ok(this.caixinhaService.getColaborator(id));
	}

	@ApiOperation(value = "Find Colaborators")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
	            value = "Page number to return (0..N)"),
	    @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
	            value = "Number of elements in a page."),
	    @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query",
	            value = "Sort criteria in this format: property(,asc|desc). " +
	                    "By default, the sort order is ascendent (asc). " +
	                    "Multiple sort criteria are accepted.")
	})
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public ResponseEntity<Page<Colaborator>> list(@ApiIgnore Pageable pageable) {
		return ResponseEntity.ok().body(this.caixinhaService.findAllColaborator(pageable));
	}

}
