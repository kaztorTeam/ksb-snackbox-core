package br.com.kaztor.geeknow.snackbox.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.kaztor.geeknow.snackbox.model.Colaborator;

public interface ColaboratorRepository extends JpaRepository<Colaborator, Long> {

}
