package br.com.kaztor.geeknow.snackbox.conf;

public enum ErrorCode {
    // NO_ERROR(0),
	// Generic Error Codes
	INTERNAL_UNEXPECTED_ERROR(0),
	ARGUMENTS_NOT_VALID(1),
	DUPLICATE_RECORD(2),
	RECORD_NOT_FOUND(3),
	NOT_ALLOWED(4),
	FILE_SYSTEM_ERROR(5);

	private final Integer value;
	
	private ErrorCode(Integer value) {
		this.value = value;
	}
	
	public Integer getValue() { return value; }
	
	public static String errorCodeKey() { return "errorCode"; }
	
	public static String errorFieldKey() { return "errorField"; }

	public static String errorMsgKey() { return "errorMsg"; }
}
