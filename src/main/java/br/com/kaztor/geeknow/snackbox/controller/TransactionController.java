package br.com.kaztor.geeknow.snackbox.controller;

import java.math.BigDecimal;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.kaztor.geeknow.snackbox.dto.CreateUpdatePaymentTransactionDTO;
import br.com.kaztor.geeknow.snackbox.dto.CreateUpdateSnackPurchaseTransactionDTO;
import br.com.kaztor.geeknow.snackbox.exceptions.RecordNotFoundException;
import br.com.kaztor.geeknow.snackbox.model.Colaborator;
import br.com.kaztor.geeknow.snackbox.model.Transaction;
import br.com.kaztor.geeknow.snackbox.service.SnackBoxService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@Api(tags = "transaction")
@RestController
@RequestMapping("/transaction")
public class TransactionController {

	@Resource
	private SnackBoxService caixinhaService;

	@ApiOperation(value = "Save a new Colaborator Payment Transaction", code = 201)
	@RequestMapping(method = RequestMethod.POST, value = "/payment")
	public @ResponseBody ResponseEntity<Transaction> create(
			@RequestBody @Valid CreateUpdatePaymentTransactionDTO transactionDTO) throws RecordNotFoundException {
		Colaborator colaborator = this.caixinhaService.getColaborator(transactionDTO.getColaboratorId());
		
		Transaction transaction = new Transaction();
		BeanUtils.copyProperties(transactionDTO, transaction);
		transaction.setDescription("Pagamento de " + colaborator.getName());
		transaction.setColaborator(colaborator);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(this.caixinhaService.createTransaction(transaction));
	}

	@ApiOperation(value = "Save a new Snack Purchase Transaction", code = 201)
	@RequestMapping(method = RequestMethod.POST, value = "/purchase")
	public @ResponseBody ResponseEntity<Transaction> create(
			@RequestBody @Valid CreateUpdateSnackPurchaseTransactionDTO transactionDTO) throws RecordNotFoundException {
		Colaborator colaborator = this.caixinhaService.getColaborator(transactionDTO.getColaboratorId());

		Transaction transaction = new Transaction();
		BeanUtils.copyProperties(transactionDTO, transaction);
		transaction.setColaborator(colaborator);
		transaction.setAmount(transaction.getAmount().multiply(new BigDecimal(-1)));

		return ResponseEntity.status(HttpStatus.CREATED).body(this.caixinhaService.createTransaction(transaction));
	}

	@ApiOperation(value = "Delete a Transaction identified by the ID")
	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		this.caixinhaService.deleteTransaction(id);
		return ResponseEntity.ok().build();
	}

	@ApiOperation(value = "Get a Transaction identified by the ID")
	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public @ResponseBody ResponseEntity<Transaction> get(@PathVariable Long id) throws RecordNotFoundException {
		return ResponseEntity.ok(this.caixinhaService.getTransaction(id));
	}

	@ApiOperation(value = "Find Transactions")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
	            value = "Page number to return (0..N)"),
	    @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
	            value = "Number of elements in a page."),
	    @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query",
	            value = "Sort criteria in this format: property(,asc|desc). " +
	                    "By default, the sort order is ascendent (asc). " +
	                    "Multiple sort criteria are accepted.")
	})
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public ResponseEntity<Page<Transaction>> list(@ApiIgnore Pageable pageable) {
		return ResponseEntity.ok().body(this.caixinhaService.findAllTransaction(pageable));
	}

}
