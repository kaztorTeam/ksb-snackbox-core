package br.com.kaztor.geeknow.snackbox.dto;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateUpdateSnackPurchaseTransactionDTO {

	@NotNull
	private BigDecimal amount;

	@NotNull
	private Date transactionDate;

	@NotNull
	private Date referenceDate;

	@Length(min = 5, max = 100)
	private String description;

	@NotNull
	private Long colaboratorId;

}
