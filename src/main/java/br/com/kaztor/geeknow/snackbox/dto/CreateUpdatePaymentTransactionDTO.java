package br.com.kaztor.geeknow.snackbox.dto;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateUpdatePaymentTransactionDTO {

	@NotNull
	private BigDecimal amount;

	@NotNull
	private Date transactionDate;

	@NotNull
	private Date referenceDate;

	@NotNull
	private Long colaboratorId;

}
