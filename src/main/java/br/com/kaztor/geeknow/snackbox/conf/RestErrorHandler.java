package br.com.kaztor.geeknow.snackbox.conf;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.kaztor.geeknow.snackbox.exceptions.RecordNotFoundException;

@ControllerAdvice(annotations = RestController.class)
public class RestErrorHandler {

	@ExceptionHandler(RecordNotFoundException.class)
	public @ResponseBody ResponseEntity<Void> processValidationError(RecordNotFoundException ex) {
		return ResponseEntity.notFound()
				.header(ErrorCode.errorCodeKey(), ErrorCode.RECORD_NOT_FOUND.getValue().toString())
				.header(ErrorCode.errorMsgKey(), ex.getMessage())
				.build();
	}

	@ExceptionHandler(Exception.class)
	@ResponseBody
	public ResponseEntity<Void> processValidationError(Exception ex) {
		ex.printStackTrace();
		return ResponseEntity.ok().header(ErrorCode.errorCodeKey(), ErrorCode.INTERNAL_UNEXPECTED_ERROR.getValue().toString())
				.header(ErrorCode.errorMsgKey(), ex.getMessage())
				.build();
	}

}
