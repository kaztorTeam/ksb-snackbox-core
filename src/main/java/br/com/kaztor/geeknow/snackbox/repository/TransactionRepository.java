package br.com.kaztor.geeknow.snackbox.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.kaztor.geeknow.snackbox.model.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

}
