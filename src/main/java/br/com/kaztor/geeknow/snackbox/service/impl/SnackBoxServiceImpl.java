package br.com.kaztor.geeknow.snackbox.service.impl;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.kaztor.geeknow.snackbox.exceptions.RecordNotFoundException;
import br.com.kaztor.geeknow.snackbox.model.Colaborator;
import br.com.kaztor.geeknow.snackbox.model.Transaction;
import br.com.kaztor.geeknow.snackbox.repository.ColaboratorRepository;
import br.com.kaztor.geeknow.snackbox.repository.TransactionRepository;
import br.com.kaztor.geeknow.snackbox.service.SnackBoxService;

@Service
public class SnackBoxServiceImpl implements SnackBoxService {

	private static final String MSG_COLABORATOR_NOT_FOUND = "Colaborator not found";
	private static final String MSG_TRANSACTION_NOT_FOUND = "Transaction not found";

	@Resource
	private ColaboratorRepository colaboratorRepository;
	@Resource
	private TransactionRepository transactionRepository;

	@Override
	public Colaborator createColaborator(Colaborator colaborator) {
		return this.colaboratorRepository.save(colaborator);
	}

	@Override
	public Colaborator updateColaborator(Colaborator colaborator) {
		return this.colaboratorRepository.save(colaborator);
	}

	@Override
	public Colaborator getColaborator(Long id) throws RecordNotFoundException {
		Colaborator colaborator = this.colaboratorRepository.findOne(id);
		if (colaborator == null) {
			throw new RecordNotFoundException(MSG_COLABORATOR_NOT_FOUND);
		}
		return colaborator;
	}

	@Override
	public Page<Colaborator> findAllColaborator(Pageable pageable) {
		return this.colaboratorRepository.findAll(pageable);
	}

	@Override
	public void deleteColaborator(Long id) {
		this.colaboratorRepository.delete(id);
	}

	@Override
	public Transaction createTransaction(Transaction transaction) {
		return this.transactionRepository.save(transaction);
	}

	@Override
	public Transaction updateTransaction(Transaction transaction) {
		return this.transactionRepository.save(transaction);
	}

	@Override
	public Transaction getTransaction(Long id) throws RecordNotFoundException {
		Transaction transaction = this.transactionRepository.findOne(id);
		if (transaction == null) {
			throw new RecordNotFoundException(MSG_TRANSACTION_NOT_FOUND);
		}
		return transaction;
	}

	@Override
	public Page<Transaction> findAllTransaction(Pageable pageable) {
		return this.transactionRepository.findAll(pageable);
	}

	@Override
	public void deleteTransaction(Long id) {
		this.transactionRepository.delete(id);
	}

}
