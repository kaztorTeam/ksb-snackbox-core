package br.com.kaztor.geeknow.snackbox.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = "id")
@Entity
@Table(name = "transaction")
public class Transaction {

	@Id
	@SequenceGenerator(name = "transactionSeq", sequenceName = "transaction_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "transactionSeq")
	@Column(name = "id")
	private Long id;

	@NotNull
	@Column(name = "amount", length = 100, nullable = false)
	private BigDecimal amount;

	@NotNull
	@Column(name = "transaction_date", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date transactionDate;

	@NotNull
	@Column(name = "reference_date", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date referenceDate;

	@Length(min = 5, max = 100)
	@Column(name = "description", length = 100)
	private String description;

	@NotNull
	@ManyToOne
    @JoinColumn(name="colaborator_id", nullable = false)
	private Colaborator colaborator;

}
