package br.com.kaztor.geeknow.snackbox.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.kaztor.geeknow.snackbox.exceptions.RecordNotFoundException;
import br.com.kaztor.geeknow.snackbox.model.Colaborator;
import br.com.kaztor.geeknow.snackbox.model.Transaction;

public interface SnackBoxService {

	/*
	 * Colaborator related methods
	 */
	Colaborator createColaborator(Colaborator colaborator);

	Colaborator updateColaborator(Colaborator colaborator);
	
	Colaborator getColaborator(Long id) throws RecordNotFoundException;
	
	Page<Colaborator> findAllColaborator(Pageable pageable);
	
	void deleteColaborator(Long id);

	/*
	 * Transaction related methods
	 */
	Transaction createTransaction(Transaction transaction);

	Transaction updateTransaction(Transaction transaction);
	
	Transaction getTransaction(Long id) throws RecordNotFoundException;
	
	Page<Transaction> findAllTransaction(Pageable pageable);
	
	void deleteTransaction(Long id);
}
