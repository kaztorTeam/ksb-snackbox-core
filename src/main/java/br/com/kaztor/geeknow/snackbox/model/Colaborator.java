package br.com.kaztor.geeknow.snackbox.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = "id")
@Entity
@Table(name = "colaborator")
public class Colaborator {

	@Id
	@SequenceGenerator(name = "colaboratorSeq", sequenceName = "colaborator_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "colaboratorSeq")
	@Column(name = "id")
	private Long id;

	@NotNull
	@Length(min = 3, max = 100)
	@Column(name = "name", length = 100, nullable = false)
	private String name;

	@NotNull
	@Length(min = 8, max = 15)
	@Column(name = "phone", length = 15, nullable = false)
	private String phone;

}
