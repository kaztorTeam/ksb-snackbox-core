package br.com.kaztor.geeknow.snackbox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SnackBoxApplication {

	public static void main(String[] args) {
		SpringApplication.run(SnackBoxApplication.class, args);
	}
}
