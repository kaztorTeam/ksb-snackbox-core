package br.com.kaztor.geeknow.snackbox.dto;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateUpdateColaboratorDTO {

	@NotBlank
	@Length(min = 3, max = 100)
	private String name;

	@NotBlank
	@Length(min = 8, max = 15)
	private String phone;

}
